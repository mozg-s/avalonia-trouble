using Avalonia;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using System.Reactive.Disposables;
using Avalonia.NETCoreMVVM.ViewModels;
using ReactiveUI;

namespace Avalonia.NETCoreMVVM.Views
{
    public sealed class LoginView : ReactiveUserControl<LoginViewModel>
    {
        public LoginView()
        {
            this.WhenActivated(disposables => { });
            AvaloniaXamlLoader.Load(this);
        }
    }
}

