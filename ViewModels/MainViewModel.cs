using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Windows.Input;
using System.Reactive.Linq;
using System.Reactive;
using ReactiveUI;
using Splat;

namespace Avalonia.NETCoreMVVM.ViewModels
{
    public class MainViewModel : ReactiveObject, IScreen
    {
        private readonly ReactiveCommand<Unit, Unit> _search;
        private readonly ReactiveCommand<Unit, Unit> _login;
        private RoutingState _router = new RoutingState();

        public MainViewModel()
        {
            _login = ReactiveCommand.Create(
                () => { Router.Navigate.Execute(new LoginViewModel()); });

            _search = ReactiveCommand.Create(
                () => { Router.Navigate.Execute(new SearchViewModel()); });
        }
        
        public RoutingState Router
        {
            get => _router;
            set => this.RaiseAndSetIfChanged(ref _router, value);
        }

        public ICommand Search => _search;

        public ICommand Login => _login;
    }
}
